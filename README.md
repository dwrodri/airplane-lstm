# Airplane Travel Forecasting with an LSTM

An exercise I did to familiarize myself better with the Keras LSTM API.
The LSTM attempts to forecast the international airline flight sales based on historical
data collected from 1945-1960.
